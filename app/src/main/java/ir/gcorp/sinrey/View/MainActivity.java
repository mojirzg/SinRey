package ir.gcorp.sinrey.View;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import ir.gcorp.sinrey.Model.ApiAdapter;
import ir.gcorp.sinrey.R;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity {

    ApiAdapter apiAdapter = new ApiAdapter();
    private static final int EXTERNAL_ESTORAGE = 100;
    private static final int OPEN_GALLERY = 99;


    ImageView sendImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button rx = findViewById(R.id.rx);
        Button async = findViewById(R.id.async);
        final Button sendFile = findViewById(R.id.send_file);
        sendImage = findViewById(R.id.send_image);


        rx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //darkhaste Rx

                apiAdapter.simpleRx(MainActivity.this, "status");

            }
        });


        async.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //darkhaste Async

                SimpleAsyncClass simpleAsyncClass = new SimpleAsyncClass();
                simpleAsyncClass.execute("status2");


            }
        });

        sendFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*ketab khune Easy Permission vase check kardan/ Porsidane Permission haye androide ke yekam
                 * in karo rahat mikone
                 * inja check mikonim ke agar taraf ejaze ro be app dade gallery o baz kone
                 * age nadade aval egaze begire
                 * vaghi gallery o baz kard khate 120 be  bad ejra mishe */
                if (EasyPermissions.hasPermissions(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
                    openGalleryIntent.setType("image/*");
                    startActivityForResult(openGalleryIntent, OPEN_GALLERY);


                } else {

                    EasyPermissions.requestPermissions(MainActivity.this, "C'mon Man", EXTERNAL_ESTORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
                }
            }
        });

    }

    //kelase Async vase frestadane data be server be surate Async
    class SimpleAsyncClass extends AsyncTask<String, Void, ApiAdapter.GeneralResponse> {

        @Override
        protected ApiAdapter.GeneralResponse doInBackground(String... strings) {
            String arg = strings[0];
            return apiAdapter.simpleAsync(arg);

        }

        @Override
        protected void onPostExecute(ApiAdapter.GeneralResponse generalResponse) {
            super.onPostExecute(generalResponse);

            Toast.makeText(MainActivity.this, generalResponse.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //in code vaghti ejra mishe ke taraf ye ax entekhab kone
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == OPEN_GALLERY && resultCode == Activity.RESULT_OK && data.getData() != null) {
            Uri uri = data.getData();
            if (EasyPermissions.hasPermissions(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                String filePath = getRealPathFromURIPath(uri, this);
                File file = new File(filePath);

                //hajme file o check mikonim age ziad gonde bashe retrofit nmifreste
                if (file.length() < 1048576) {

                    sendImage.setImageURI(uri);


                    //axo upload mikonim
                    apiAdapter.upload(this, file);


                } else {

                    Toast.makeText(MainActivity.this, "File too large", Toast.LENGTH_SHORT).show();

                }
            }
        }
    }


    //in method vaghti taraf ye axo az gallery entekhab kard migire address e (path) un file o bar migardune
    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }
}
