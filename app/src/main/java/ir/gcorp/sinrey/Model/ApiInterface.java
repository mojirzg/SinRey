package ir.gcorp.sinrey.Model;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

/*
* baraye har darkhast niaz be se ta chiz hast
* Ye kelas baraye darkhast
* ye kelas baraye Pasokhe Server
* ye interface baraye Ertebate in do ta
*
* Do taye aval to kelase ApiInterface hast
* va kole Interface ha ham inja tu ye Interface Madar gozashte shode
* */

public interface ApiInterface {

    interface SimpleRxInterface {
        @POST("api.php")
        Observable<ApiAdapter.GeneralResponse> getResponse(@Body ApiAdapter.SimpleRxRequest simpleRxRequest) ;
    }

    interface SimpleAsyncInterface {
        @POST("api.php")
        Call<ApiAdapter.GeneralResponse> getResponse(@Body ApiAdapter.SimpleAsyncRequest simpleAsyncRequest) ;
    }

    interface SendFileInterface {
        @Multipart
        @POST("upload.php")
        Observable<ApiAdapter.GeneralResponse> uploadAttachment(@Part MultipartBody.Part filePart) ;
    }


    }


