package ir.gcorp.sinrey.Model;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.annotations.SerializedName;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/* 3 no mozakhrafat inja hast kolan
ye method bare anjame kare darkhast
ye kelas ke be shekl Json mishe mire vase Server ke mamulan ba ""Request
minevisam va ye kelas ke Json ii ke az Server miad Rikhte mishe tush
kheyli moheme ke Header ha yeki bashe

maslaan data ye kelase SimpleRxRequest bayad DAGHIGHAN INJURI bashe

{
    "success" : "true",
    "message" : "some text"
}

vagarna Error mide ya data ro kamel nmigire

*
* */

public class ApiAdapter {
    private Retrofit retrofit = RetrofitClient.getClient();


    //region Simple Data - Rx Pattern
    public void simpleRx(final Context context,String state) {
        ApiInterface.SimpleRxInterface apiInterface = retrofit.create(ApiInterface.SimpleRxInterface.class);

        apiInterface.getResponse(new SimpleRxRequest(state))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GeneralResponse>() {
                    @Override
                    public void onCompleted() {
                        //vaghti heme chi tamum she in code ejra mishe
                        //bedard nmikhore ziad chon javab to onNext miad
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Toast.makeText(context,"Error Something went Wrong",Toast.LENGTH_SHORT).show();


                    }

                    @Override
                    public void onNext(GeneralResponse response) {
                        Toast.makeText(context,response.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });


    }


    class SimpleRxRequest {
        @SerializedName("state")
        String state ;

        public SimpleRxRequest(String state) {
            this.state = state;
        }
    }

    //endregion


    //region Simple Data - Async

    public GeneralResponse simpleAsync (String state) {
        ApiInterface.SimpleAsyncInterface apiInterface = retrofit.create(ApiInterface.SimpleAsyncInterface.class);

        Call<GeneralResponse> call = apiInterface.getResponse(new SimpleAsyncRequest(state));
        try {
            return call.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("retrofit", "error");
        }

        return null;


    }

    public class SimpleAsyncRequest {

            @SerializedName("state")
            String state ;

            public SimpleAsyncRequest(String state) {
                this.state = state;
            }
    }

    //endregion


    //region Send File
    public void upload(final Context context, File file) {

        ApiInterface.SendFileInterface apiInterface = retrofit.create(ApiInterface.SendFileInterface.class);

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        apiInterface.uploadAttachment(filePart)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GeneralResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                    }

                    @Override
                    public void onNext(GeneralResponse response) {
                        Log.i("applog", response.getMessage());
                        Toast.makeText(context, response.message, Toast.LENGTH_SHORT).show();
                    }
                });


    }


    //endregion


    public class GeneralResponse {
        @SerializedName("success")
        Boolean success;
        @SerializedName("message")
        String message;

        public Boolean getSuccess() {
            return success;
        }

        public void setSuccess(Boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }


}
